'use strict';

/**
 * @ngdoc function
 * @name ingTradingApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ingTradingApp
 */
angular.module('ingTradingApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
