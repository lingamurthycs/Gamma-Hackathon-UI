'use strict';

/**
 * @ngdoc function
 * @name ingTradingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ingTradingApp
 */
angular.module('ingTradingApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
